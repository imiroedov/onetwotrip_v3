package services

import (
	"time"
	"errors"
	"github.com/garyburd/redigo/redis"
)

func NewConnection(network, address, password string) (redis.Conn, error) {
	return dial(network, address, password)
}

func NewConnectionPool(size int, network, address, password string) (*redis.Pool, error) {
	pool := &redis.Pool{
		MaxIdle:     size,
		IdleTimeout: 240 * time.Second,
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
		Dial: func() (redis.Conn, error) {
			return dial(network, address, password)
		},
	}

	conn := pool.Get()
	defer conn.Close()
	data, err := conn.Do("PING")

	if err != nil || data == nil {
		return nil, err
	}

	if data != "PONG" {
		return nil, errors.New("PONG is not")
	}

	return pool, nil
}

func dial(network, address, password string) (redis.Conn, error) {
	c, err := redis.Dial(network, address)
	if err != nil {
		return nil, err
	}

	if password != "" {
		if _, err := c.Do("AUTH", password); err != nil {
			c.Close()
			return nil, err
		}
	}

	return c, err
}
