package client

import (
	"fmt"
	"sync"
	"time"
	"strings"
	"math/rand"
	"onetwotrip_v3/services"
	"github.com/garyburd/redigo/redis"
)

const (
	Undefined RedisClientType = iota
	Generator
	Listener
)
type RedisClientType int

type RedisClient struct {
	wg     sync.WaitGroup
	pool   *redis.Pool
	index  int64
	abort  chan struct{}
	status RedisClientType
	msgQue string
	errSet string
}

func NewRedisClient(network, address, password string) (*RedisClient, error) {
	rand.Seed(time.Now().UnixNano())

	ncp, err := services.NewConnectionPool(10, network, address, password)
	if err != nil {
		return nil, err
	}

	rc := &RedisClient{
		pool: ncp,
		abort: make(chan struct{}),
		status: Undefined,
		errSet: "set:errors",
		msgQue: "queue:messages",
	}

	if err := rc.setName(); err != nil {
		return nil, err
	}

	return rc, nil
}

func (rc *RedisClient) setName() error {
	conn := rc.pool.Get()
	if err := conn.Err(); err != nil {
		return err
	}
	defer conn.Close()

	var script = redis.NewScript(0, `
    	local name = redis.call('INCR', 'counter')
    	redis.call('SETNX', 'leader', name)
        return name
    `)

	idx, err := redis.Int64(script.Do(conn))
	if err != nil {
		panic(err)
	}

	rc.index = idx
	return nil
}

func (rc *RedisClient) setMode(mode RedisClientType) {
	rc.stop()
	rc.wg.Add(1)

	switch mode {
	case Listener:
		go rc.listener()
		rc.status = Listener
		fmt.Println(rc.index, "-> Listener")
	case Generator:
		go rc.generator()
		rc.status = Generator
		fmt.Println(rc.index, "-> Generator")
	}
}

func (rc *RedisClient) generator() {
	defer func() {
		rc.wg.Done()
		rc.status = Undefined
	}()

	conn := rc.pool.Get()
	if err := conn.Err(); err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	defer conn.Close()

	tick := time.NewTicker(500 * time.Millisecond)
	defer tick.Stop()

	for {
		select {
		case <-tick.C:
			if err := conn.Send("LPUSH", rc.msgQue, rc.newMessage()); err != nil {
				fmt.Println("ERROR:", err)
				return
			}
			conn.Flush()
		case <-rc.abort:
			return
		}
	}
}

func (rc *RedisClient) listener() {
	defer func() {
		rc.wg.Done()
		rc.status = Undefined
	}()

	conn := rc.pool.Get()
	if err := conn.Err(); err != nil {
		fmt.Println("ERROR:", err)
		return
	}
	defer conn.Close()

	for {
		select {
		case <-rc.abort:
			return
		default:
			if val, err := redis.StringMap(conn.Do("BRPOP", rc.msgQue, 1)); err != nil {
				if err != redis.ErrNil {
					fmt.Println("ERROR:", err)
					return
				}
			} else {
				msg := fmt.Sprintf("%d : %s", rc.index, val[rc.msgQue])
				fmt.Println(msg)

				if rand.Float64() < 0.05 {
					if err := conn.Send("SADD", rc.errSet, msg); err != nil {
						fmt.Println("ERROR:", err)
						return
					}
					conn.Flush()
				}
			}
		}
	}
}

func (rc *RedisClient) heartbeat() {
	conn := rc.pool.Get()
	if err := conn.Err(); err != nil {
		panic(err)
	}
	defer conn.Close()

	tick := time.NewTicker(1 * time.Second)
	defer tick.Stop()

	for {
		<-tick.C

		idx, err := redis.Int64(conn.Do("GET", "leader"))
		if err != nil && err != redis.ErrNil {
			panic(err)
		}

		if idx == rc.index {
			if rc.status != Generator {
				rc.setMode(Generator)
			}

			if err := conn.Send("SETEX", "leader", 5, rc.index); err != nil {
				panic(err)
			}
			conn.Flush()
		} else {
			if rc.status != Listener {
				rc.setMode(Listener)
			}

			if err := conn.Send("SETNX", "leader", rc.index); err != nil {
				panic(err)
			}
			conn.Flush()
		}
	}
}

func (rc *RedisClient) stop() {
	if rc.status != Undefined {
		rc.abort <- struct{}{}
		rc.wg.Wait()
	}
}

func (rc *RedisClient) newMessage() string {
	return time.Now().String()
}

func (rc *RedisClient) Do() {
	rc.heartbeat()
}

func (rc *RedisClient) ShowErrors() {
	conn := rc.pool.Get()
	if err := conn.Err(); err != nil {
		panic(err)
	}
	defer conn.Close()

	var script = redis.NewScript(1, `
    	local lst = redis.call('SMEMBERS', KEYS[1])
    	redis.call('DEL', KEYS[1])
        return lst
    `)

	val, err := redis.Strings(script.Do(conn, rc.errSet))
	if err != nil {
		panic(err)
	}

	if len(val) > 0 {
		fmt.Println(strings.Join(val, "\n"))
	}
}