package main

import (
	"fmt"
	"flag"
	"onetwotrip_v3/client"
)

var host = flag.String("host", "", "host:port")
var pass = flag.String("password", "", "password")
var show = flag.Bool("getErrors", false, "show errors")

func main() {
	flag.Parse()

	cli, err := client.NewRedisClient("tcp", *host, *pass)
	if err != nil {
		fmt.Println("ERROR:", err)
	}

	if !*show {
		cli.Do()
	} else {
		cli.ShowErrors()
	}
}